package spark

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
  * spark编程固定模型
  */
object SparkDemo {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local[*]")
      .setAppName("spark_demo")
    /**
      * Spark 2. 0 引入 SparkSession 封装 了 SparkContext 和 SQLContext，
      * 并且会在builder 的 getOrCreate 方法 中 判断 是否有符合要求的SparkSession存在，
      * 有则使用，没有则进行创建
      */
    val spark = SparkSession.builder.config(conf).getOrCreate()
    val sc = spark.sparkContext
    val sqlContext = spark.sqlContext

    /**
      * 设置spark程序的日志打印级别为warn，以便于调试
      */
    sc.setLogLevel("warn")

    /**
      * RDD DataFrame DataSet的创建及获取
      */
    /**
      * 具体的业务逻辑处理
      */
    /**
      * 最后关闭sparkSession
      */
    // close 函数底层就是调用了stop函数
    spark.close()
//    spark.stop()
  }
}
