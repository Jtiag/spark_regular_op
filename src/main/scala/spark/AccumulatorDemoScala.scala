package spark

import org.apache.spark.sql.SparkSession

object AccumulatorDemoScala {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("accumulator")
      .master("local[*]")
      .getOrCreate()

    val sc = spark.sparkContext

    val accum = sc.accumulator(0,"accumulator1.x")

    sc.parallelize(Array(1,2,3,4,5)).foreach(x => accum +=x)

    println(accum.value)
  }
}
