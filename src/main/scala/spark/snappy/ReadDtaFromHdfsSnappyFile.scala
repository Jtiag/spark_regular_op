package spark.snappy

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

object ReadDtaFromHdfsSnappyFile {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("test")
      .setMaster("local")
      .set("spark.io.compression.codec", "org.apache.spark.io.SnappyCompressionCodec")
    val sc = new SparkContext(conf)
    //    val StringRDD = sc.textFile("hdfs://cdhmanager:8020/flumedata/yunzhiDeviceLog200/2018-05-04/yunzhiDeviceLog200.1525422897278.snappy")
    val StringRDD = sc.textFile("hdfs://cdhmanager:8020/flumedata/test.txt")
    val strings = StringRDD.collect()


//    StringRDD.foreach(word => println(word))
    for (word <- strings) {
      println(word)
    }

  }
}
