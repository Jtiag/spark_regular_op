package spark.streaming.readkafka

object ConfPicks {
  val HOST = "cdhmanager"
  val HDFS_URL = "hdfs://"+HOST+":8020"
  val MASTER = "local[*]"
  val KAFKA_BROKER_LIST = HOST+":9092"
  /**
    * 多个topic以"，"分割
    */
  val KAFKA_TOPICS = "kafka_flume_hdfs1"
}
