import scala.io.Source

object ReadLocalFile {
  def main(args: Array[String]): Unit = {
    val fileSrc = Source.fromFile("C:\\Users\\Administrator\\Desktop\\localfile.txt")
    val mkString = fileSrc.mkString
    val strings = mkString.split("::")
    println(strings(0))
  }
}
