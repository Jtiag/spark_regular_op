import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述:
 * 随机生成文件
 *
 * @author jiantao7
 * @create 2018-05-07 14:38
 */
public class GenerationFile {
    public static void main(String[] args) throws IOException {
        String filename = "newFile";

        File file = new File(filename);
        List<File> fileNames = new ArrayList<File>();
        int fileNum = 64;
        for (int i = 0; i < fileNum; i++) {
            File f = new File(filename + "_" + i);
            fileNames.add(f);
            if (!file.exists()) {
                System.out.println("文件不存在");
                try {
                    boolean isfileNewFile = file.createNewFile();

                    if (isfileNewFile) {
                        System.out.println("创建成功");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        FileOutputStream fileOutputStream = null;
        try {
            for (int i = 0; i < fileNum; i++) {
                int frans = 30000;
                fileOutputStream = new FileOutputStream(fileNames.get(i));
                for (int j = i*frans; j < (i+1)*frans; j++) {
                    fileOutputStream.write((j + " " + System.currentTimeMillis() + "\n").getBytes());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
    }
}