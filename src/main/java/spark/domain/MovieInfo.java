package spark.domain;

/**
 * 描述:
 * movie info
 *
 * @author jiantao7
 * @create 2018-05-17 17:06
 */
public class MovieInfo {
    /**
     * MovieID:: Title:: Genres
     */
    private String movieId;
    private String movieName;
    private String genres;

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }
}
