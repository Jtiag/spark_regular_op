package spark.domain;

/**
 * 描述:
 * 评分相关
 *
 * @author jiantao7
 * @create 2018-05-17 17:08
 */
public class RatingsInfo {
    /**
     * UserID:: MovieID:: Rating:: Timestamp
     */
    private String userId;
    private String movieId;
    private int rating;
    private long timestamp;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}