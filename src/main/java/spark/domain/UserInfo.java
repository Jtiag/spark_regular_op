package spark.domain;

/**
 * 描述:
 * user info
 *
 * @author jiantao7
 * @create 2018-05-17 16:53
 */
public class UserInfo {
    /**
     * UserID:: Gender:: Age:: Occupation:: Zip-code
     */
    private String userId;
    private String gender;
    private int age;
    private String occupation;
    private String zipCode;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}