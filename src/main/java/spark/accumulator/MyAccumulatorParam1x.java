package spark.accumulator;

import org.apache.spark.AccumulatorParam;

/**
 * 描述:
 *
 * @author jiantao7
 * @create 2018-05-21 10:43
 */
public class MyAccumulatorParam1x implements AccumulatorParam<Integer>{

    @Override
    public Integer addAccumulator(Integer t1, Integer t2) {
        return t1*t2;
    }

    @Override
    public Integer addInPlace(Integer r1, Integer r2) {
        return r1*r2;
    }

    @Override
    public Integer zero(Integer initialValue) {
        return 1;
    }
}