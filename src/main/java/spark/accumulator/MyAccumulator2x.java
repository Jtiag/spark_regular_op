package spark.accumulator;

import org.apache.spark.util.AccumulatorV2;

/**
 * 描述:
 *
 * @author jiantao7
 * @create 2018-05-21 11:16
 */
public class MyAccumulator2x extends AccumulatorV2<Integer,Integer> {
    private int result = 0;
    @Override
    public boolean isZero() {
        return result==0;
    }

    @Override
    public AccumulatorV2<Integer, Integer> copy() {
        MyAccumulator2x myAccumulator2x = new MyAccumulator2x();
        myAccumulator2x.result = this.result;
        return myAccumulator2x;
    }

    @Override
    public void reset() {
        new MyAccumulator2x();
    }

    @Override
    public void add(Integer v) {
        this.result += v;
    }

    @Override
    public void merge(AccumulatorV2<Integer, Integer> other) {
        this.result += other.value();
    }

    @Override
    public Integer value() {
        return this.result;
    }
}