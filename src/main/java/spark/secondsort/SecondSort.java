package spark.secondsort;

import scala.math.Ordered;

import java.io.Serializable;

/**
 * 描述:
 * 根据ratings 和timestamp进行二次排序 时间相等后根据ratings来排序
 *
 * @author jiantao7
 * @create 2018-05-17 13:50
 */
public class SecondSort implements Serializable, Ordered<SecondSort> {

    private int ratings;
    private long timestamp;

    public SecondSort(long timestamp, int ratings) {
        this.timestamp = timestamp;
        this.ratings = ratings;
    }

    @Override
    public int compare(SecondSort that) {
        if ((this.timestamp - that.timestamp) != 0)
        {
            return (int) (this.timestamp - that.timestamp);
        }else {
            return this.ratings - that.ratings;
        }
    }

    @Override
    public boolean $less(SecondSort that) {

        if (this.compare(that) < 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean $greater(SecondSort that) {
        if (this.compare(that) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean $less$eq(SecondSort that) {
        if (this.compare(that) <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public boolean $greater$eq(SecondSort that) {
        if (this.compare(that) >= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public int compareTo(SecondSort that) {
        return this.compare(that);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecondSort that = (SecondSort) o;

        if (ratings != that.ratings) return false;
        return timestamp == that.timestamp;
    }

    @Override
    public int hashCode() {
        int result = ratings;
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        return result;
    }

    public int getRatings() {
        return ratings;
    }

    public long getTimestamp() {
        return timestamp;
    }
}