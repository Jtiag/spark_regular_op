package spark;

/**
 * 描述:
 * 生成模拟的数据
 * 直接到：https://grouplens.org/datasets/movielens/下载数据
 * @author jiantao7
 * @create 2018-05-16 16:26
 */
public class GenerationData {
    public static void main(String[] args) {
        String path = "C:\\Users\\Administrator\\Desktop";
        generationUserData(path);
        generationMovieData(path);
        generationRatingsData(path);
    }

    private static void generationRatingsData(String path) {
        /**
         * UserID:: MovieID:: Rating:: Timestamp
         */
    }

    private static void generationMovieData(String path) {
        /**
         * MovieID:: Title:: Genres
         */
    }

    private static void generationUserData(String path) {
        /**
         * UserID:: Gender:: Age:: Occupation:: Zip-code
         */
    }
}